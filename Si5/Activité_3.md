__Andy Sabathier__<br>
__Bts SIO SLAM__<br>
__28/04/2019__<br>

# <center>__*Activité 3 : Installation MariaDb et Adminer*__</center>

### <center>__*Etape 1: Installation de MariaDb*__</center>
<br>

Pour installer un serveur MariaDB. Il faudra écrire ces commandes 

        sudo apt update
        sudo apt install mariadb-server
        sudo mysql_secure_installation
Après cette commande plusieurs questions sont posées <br>
voulez vous changez de mot de passse ?  Y <br>
nouveau mot de passe : 20 <br>
confirmer : 20 <br>
enlever les utilisateurs anonyme? Y <br>
interdir la connexion à distance? Y <br>
supprimer la base de donné de test de mariadb? n <br>
recharger les tables de privilège maintenant ? y <br>

Pour lancer MariaDb

        sudo systemctl start mariadb
Arreter MariaDb
        
        sudo systemctl stop mariadb
        
Redémarrer MariaDb

        sudo systemctl restart mariadb
        
Vérifier que le serveur soit opérationnel :

        sudo mysql -u root -p
        MariaDB --> SHOW DATABASES;

Les fichiers de conf sont donnée dans le fichier /etc/mysql/my.cnf <br>
les 2 fichiers sont /etc/mysql/conf.d  /etc/mysql/mariadb.conf.d


### <center>__*Etape 2: Installation du module PHP*__</center>

Pour installer php commencer par créez une machine ou il y a apache2 d'installer.<br>

Les paquets à installer sont  php5-common / libapache2-mod-php5 / php5-cli
<br>
Pour installer ces paquets taper la commande suivante

         sudo apt-get install php7.0
         sudo apt-get install php7.0-common libapache2-mod-php7.0 php7.0-cli
         sudo apt-get install php7.0-mysql php7.0-curl
         sudo systemctl restart apache2

pour changer le nom et ouvrir le fichier php faire : 
        
        sudo mv moi.html moi.php
        
allez sur firefox et marqué localhost/moi.php <br>
la machine server est la machine apache_php7.0 et la machine cliente est la machine de l'os hote  <br>

### <center>__*Etape 3: Installation de l'application Adminer*__</center>

pour télécharger l'application adminer allez sur une machine ou il y a apache et php et faire :

        sudo wget /var/www/html 'https://github.com/vrana/adminer/releases/download/v4.7.1/adminer-4.7.1.php'

changer le nom de index.html en adminer.php

        sudo mv adminer4.7.1.php adminer.php
        
pour tester son bon fonctionnement allez sur firefox et marqué localhost/adminer.php
si ça vous affiche la page de adminer c'est bon. <br>
la machine cliente et serveur est la meme c'est la Vm de apache_php7.0 <br>


### <center>__*Etape 4: Configuration réseau et communication*__</center>

les deux options de réseau qui fonctionnent sont accès par pont, réseau privé d'hôte et réseau interne <br>

pour tester que les 2 machines soient connecté faire la commande 
        
        ip a
        
si l'adresse ip est la meme sur les 2 machines changer l'adresse mac d une des machines <br>
sinon faire un ping sur la machine mariadb de l'adresse ip de la machine apache_php7.0 et vise versa <br>

pour se connecter à la base de donnée allez sur le navigateur de la machine mariadb et marqué dans la barre de recherche :
        
        adresse-ip-machine_apache-adminer:80(c'est le port d'apache par défault)/adminer.php
        
il vous demande des champs a remplir pour vous connecter à la base de donnée faire comme moi:

        Systèmes: mysql
        server: addresse-ip-machine-mariadb
        utilisateur: root
        mot de passe: mdp-de-root
        base de donnée: ne rien remplir 
        

le premier problème rencontrer est d'avoir eu le message d'erreur: <br>
connection failed <br>
Pour résoudre ce probleme il faut allez dans un fichier de conf de mariadb:

        sudo gedit /etc/mysql.mariadb.conf.d/50-server.cnf

et mettre la ligne "bind-address" en commentaire avec un #.

Le second probleme est d'avoir l'erreur: "host 'adresse_ip' is not allowed to connect to this mysql server"<br>
ce qui veut dire que l'utilisateur root n'as pas les droits pour accéder à la base de donné.<br>
il faut donc créez un utilisateur avec tous les droits <br>

faire en ligne de commande:

        sudo mysql -u root -p
        
une fois avoir fais cette commande vous arrivez sur une entrée avec écrit Mariadb[none]
Tapez 

        use mysql 

il y a maintenant écrit Mariadb[mysql] puis tapez les commandes sql suivante pour créez un utilisateur 

        CREATE USER 'nom-user'@'adresse-ip-machine-apache-php' IDENTIFIED BY 'mdp-user';
        GRANT ALL PRIVILEGES ON *.* TO 'nom-user'@'adresse-ip-machine-apache-ph' with grant option;
        FLUSH PRIVILEGES;
        EXIT;
        
une fois avoir réaliser ses commandes c'est à dire avoir créez un utilisateur avec tous les droits et avoir validez les changements avec FLUSH,<br> 
restart les serveur apache et mariadb des deux machines.<br>

        sudo systemctl restart apache2
        sudo systemctl restart mariadb


Si vous arrivez à vous connecter à la base de donné en mettant pour utilisateur le nouvel utilisateur et le bon mdp,<br>
vous devriez pouvoir vous connecter à la base de donnée.<br>
        
### <center>__*Etape 5: Production d'un diagramme réseau de la situation réaliser*__</center>

![](Partie5_schea_réseau.PNG)


