<strong>Andy Sabathier <br>
Bts Slam 1 <br>
25/02/2019 <br></strong>

# <center><span style='color:red' >__*<u>Activité 2: Administration Système Unix*__</span></center></u>
<br>

### <center><span style='color:red' >__*<u>Etape 0*__</span></center>
<br></u>
<strong>
1) Contenue de fichier sources.list dans Terminal:

    cat /etc/aptsources.list

La version utilisé est 9.7.0 et le nom est stretch

2)Mettre à jour la liste des packets de la distribution courante

    sudo apt-get update
    
3)mettre à jour tous les packets installé

    sudo apt-get dist-upgrade

4)l'ordre d'éxécution est importante pour update c'est la liste que l'on modifie et pour dist-upgrade c'est tous les packets de la liste .

### <center><span style='color:red' >__*<u>Etape 1*__</span></center>
<br></u>

1) Existence de apache 2 

     dpkg -l | grep apache2
    
pour tester qu'il est opérationnel allez sur firefox et marqué localhost.
Si une page web s'affiche avec écrit "It works" c'est bon.

2)Chercher si un packet Deian est déjà installé

    dpkg -l nom-du-paquetage

3) Installer un packet Debian 

    sudo apt-get install nom-du-paquetage
    
4) commandes pour démarrer, arrêter, redémarrer le serveur.
Pour arrêter apache2 :

    sudo systemctl stop apache2
    
 Pour lancer apache2 : 
 
    sudo systemctl start apache2
    
Pour relancer apache2 : 

    sudo systemctl restart apache2
    
### <center><span style='color:red' >__*<u>Etape 2*__</span></center>
<br></u>

1) Existence du groupe www-data 

    cat /etc/group
2)le propriétaire des fichiers dans var/www

    ls -l
Le propriétaire est root

3)donner les droits d'accès au groupe www-data pour voir tous les fichiers de var/www

     sudo chown -R www-data: *
    
4)Quelle droit à le groupe www-data sur les fichiers de var/www

    ls -l

il à les droits d'écriture 

### <center><span style='color:red' >__*<u>Etape 3*__</span></center>
<br></u>
 
 1) Créer un nouvel utilisateur
 
    sudo useradd lucky -g www-data -d /home/lucky -m
    sudo passwd lucky
    
Ajoutez lucky au groupe www-data

    sudo adduser lucky www-data

    
2)
les droits de lucky droit de lecture 

    sudo touch fichier.txt
on ne peut pas créer un fichier car lucky n'a pas les droits .



3)modifier les droits de www-data 

    sudo chown -R lucky:www-data /var/www
    sudo chmod 775 -R var/www

4) fichier html

    echo andy sabathier >> touch moi.html   
    
    
### <center><span style='color:red' >__*<u>Etape 4*__</span></center>
<br></u>

pour créer un site html via son dossier personnel public_html

    mkdir ~/public_html
    echo Andy Sabathier > ~/public_html/moi.html
    chmod -R 755 ~/public_html
    
    sudo a2enmod userdir
    sudo systemctl reload apache2

aller sur internet marquez localhost/~lucky
et si il s'affiche votre page ça marche 


### <center><span style='color:red' >__*<u>Etape 5*__</span></center>
<br></u>

1) Commande utilisé pour voir l'arborescence des processus

    sudo pstree


apache2 est actif 
<br>

2)tuer le processus apache2 

    sudo killall apache2
    
3)surveiller si le processus est éteint 

    sudo pstree | grep apache2
    
regardez si apache a disparue de la liste 


    

