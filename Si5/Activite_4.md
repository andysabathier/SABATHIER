_*Andy Sabathier*_ <br>
_*BTS Sio SLAM*_ <br>
_*09/05/2019*_ <br>


## _**Activité 4 : Wireshark et analyse du trafic réseau**_


### _**Etape 1 : Installer Wireshark**_

Pour télécharger et installer Wireshark suivre la démarche ci dessous :

Allez sur ce lien pour télécharger <a href="http://www.siteduzero.com">Wireshark</a>

Wireshark est un analyseur de paquets libre et gratuit. Il est utilisé dans le dépannage et l'analyse de réseaux informatiques.<br>
Il surveille toutes les trames pour une ip donnée.<br>

Pour installer wireshark suivez les étapes importantes suivantes<br>

cochez tous(ne pas oubliez de regarder dans tools et dans plugins)

![](images_acti_4/Coche_all.PNG)

![](images_acti_4/coche.PNG)

![](images_acti_4/place_install.PNG)

![](images_acti_4/installation.PNG)

### _**Etape 2 : Traffic HTTP**_

Un des clients HTTP déjà installé dans la machine hôte est firefox par exemple l'adresse ip 172.16.96.42. <br>

un serveur http est apache avec l'adresse ip 95.128.72.25 sur ma machine hote sur le port 80. <br>

Après avoir télécharger une image sur firefox voici le résultat de la capture
avec une requête et une réponse:

![](images_acti_4/protocol_http.PNG)
on peut donc voir un envoie de requête du client et une réponse du serveur http. <br>


### _**Etape 3 : Traffic FTP**_

un client ftp déjà installé est fizilla, ip du client est 172.16.96.42 sur la machine hote <br>

Le serveur FTP a cette ip : 146.66.113.185 avec le port 21

![](images_acti_4/protocol_ftp.PNG)

On peut voir une requête envoyé du client FTP avec l'adresse ip : 172.16.96.42 et une réponse du serveur FTP avec l'ip 146.66.113.185 <br>

### _**Etape 4 : Traffic SSH**_

Un client SSH est MobaXterm avec comme ip : 172.16.96.42 sur ma machine hôte <br>

un serveur ssh comme open SSH avec comme ip : 195.154.179.210 port 22

![](images_acti_4/protocol_ssh.PNG)

On peut voir qu'il y a une communication client serveur


