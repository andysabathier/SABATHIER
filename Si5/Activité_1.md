Andy Sabathier<br>
Bts SIO SLAM<br>
25/02/2019



# __*<span style='color:red'><u> Introduction à la virtualisation </u></span>*__

1. __Installer VirtualBox logicielle d'hypervision.__
2. __Créer une première machine virtuelle avec un système d'exploitation Linux Debian.__
3. __Configuration de sorte à avoir accès à internet.__
4. __Créer un dossier partagé.__
5. __faire en sorte que les 2 machines puisse discuter.__


#### __<span style='color:red'>Installer VirtualBox logicielle d'hypervision</span>__

télécharger Virtualbox 6.0.4 :
* [Windows host](https://download.virtualbox.org/virtualbox/6.0.4/VirtualBox-6.0.4-128413-Win.exe)
* [OS X host](https://download.virtualbox.org/virtualbox/6.0.4/VirtualBox-6.0.4-128413-OSX.dmg)
* [Linux Distribution](https://www.virtualbox.org/wiki/Linux_Downloads)
* [Solaris hosts](https://download.virtualbox.org/virtualbox/6.0.4/VirtualBox-6.0.4-128413-SunOS.tar.gz)

Mon système d'exploitation est Windows 10<br>
Pour autoriser l'ordinateur à utiliser une machine virtuelle, allez dans le BIOS de votre pc (la manière d'y aller et de faire la manipulation diffère selon les ordinateurs).<br>
Mon pc est un HP:<br>
pour accédez au BIOS appuyer sur F10 en démarrant l'ordinateur
Allez dans l'onglet sécurité, puis technologie de virtualisation ( activer ).
Réappuyer sur F10 pour sauvegarder et quitter.<br>
VirtualBox met en oeuvre une virtualisation totale.

#### __<span style='color:red'>Créer une première machine virtuelle avec un système d'exploitation Linux Debian</span>__

Téléchargé l'ISO Debian 9.7.0 :
* [image ISO "netinst" pour PC 64 bits ](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-9.7.0-amd64-netinst.iso)
* [image ISO "netinst" pour PC 32 bits](https://cdimage.debian.org/debian-cd/current/i386/iso-cd/debian-9.7.0-i386-netinst.iso)

Pour installer l'iso Debian ouvrez VirtualBox<br>
Cliquez sur Nouvelle (icône bleue)
1. remplisser la 1 ère page comme ci-dessous.
2. Puis mettre 2048 Mio de RAM (si vous avez 8go de RAM ou plus)
3. Créer un disque dur virtuelle maintenant
4. VDI (Images de Disque VirtualBox)
5. Dynamiquement alloué
6. Choisissez le nom de dossier voulue et 8 Gio ou plus en espace autoriser

lancer Debian18/20 une fois créer

* Choisissez l'ISO Debian précédement télécharger
* Vous arriverez à une sorte de BIOS pour installer Debian
* Install 
* Puis choisissez la langue Française ( 3 reprises pour langues, pays, clavier)
* votre Nom de machine puis continuer
* Pour le nom de domaine laisser vide
* Pour plus de sécurité ne pas mettre de mot de passe root
* choisir un nom complet nouvel utilisateur
* puis un mot de passe (pour mettre des chiffres pensé à réactiver le ver-num)
* confirmation du mot de passe
* Assisté- utilisé un disque entier
* SCSI (valider)
* Tout dans une seule partition
* Terminer le partionement et appliquer les changements
* Faut-il appliquer les changements sur les disques? (OUI)

après un petit temps de chargement faire: 

* Il nous propose un miroir comme source d'installation des paquets. On en choisira un "à la main" plus tard, donc on va dire non.
*  On refuse également de participer à l'étude statistique .
*  il y aura une nouvel fois un temps de chargement 
*  pendant le téléchargement il y aura une page avec les logicielles à téléchargé(image)
*  après un nouveau temps de chargement accepter le téléchargement de GRUB
*  périphérique ou sera installé le programme de démarrage : /dev/sda
*  Installation terminée:  Continuer

#### __<span style='color:red'>Configuration de sorte à avoir accès à internet</span>__

j'ai été automatiquement connecté à internet mais si ça ne marche pas allé sur virtualBox 
pour vérifier l'accès à internet allé sur le terminale mettre ping site_internet<br>
exemple:<br> ping amazon.com

Cette procédure ne permet pas l'accès à la machine virtuelle par la machine hôte.
le moyen de voir l'accès à la machine virtuelle par la machine hôte est :

* aller sur le terminale et marqué "ip a" puis noter l'addresse ipv4 de enp0s3: 10.0.2.15
* ouvrir le cmd de windows puis marquer: ping 10.0.2.15

Si il y a une réponse la machine hôte à accès à la machine virtuelle


#### __<span style='color:red'>Créer un dossier partagé</span>__

1. Pour la création d'un dossier partagé il faut commencer par se rendre sur virtualBox
2. Puis allé dans configuration , puis dossiers partagés
3. Pour ajouter un dossier, cliquez sur l’icône en haut à droite, le petit dossier bleu avec une croix verte dessus
4. Une fenêtre s’affiche, vous demandant le nom de partage du dossier, son chemin, et des options supplémentaires.(essayer de choisir un nom de dossier assez simple et choisir comme chemin le bureau par exemple)
5. Puis coché permanents et automatique ( pour éviter de refaire la manipulation à chaque démarage du pc)

<br>
Il faut maintenant passer sur Debian pour monter le fichier.
Pour ce faire rendez vous sur le terminale de Debian:

* créez un fichier ou vous voulez racine (mkdir /partage) ou répertoire personnel(mkdir ~/partage) 
* entrez cette commande pour finaliser la création du dossier de partage :<pre><code>sudo mount  -t   vboxsf   nom-dossier-partagé-dans-virtualbox   /partage</code></pre>

__explication de ce que signifie la commande:__

* La commande “mount” permet de “monter” un dossier. Nous le rendons accessible au travers d’un autre dossier.
* " -t " permet de définir le type de dossier partagé
* "vboxsf" est le type de dossier que nous souhaitons définir. Ce type est exclusivement réservé à VirtualBox.
* "nom-dossier-partagé-dans-virtualbox" Lors de la création de votre dossier partagé, vous deviez entrer un nom de dossier pour le partage. Vous devez donc réécrire ce nom ici, pour que la jointure se fasse.
* "/partage"  Vous devez mettre le chemin complet du dossier local que vous avez créé au préalable.

<br>
Si cela produit un beug avec "vboxsf" suivre cette marche à suivre :
Il faudra installer les additions invités :

* sur Debian aller cliquez sur périphérique puis insérer l'image cd des additions invités ( Lancer  puis Ok )
* Allez sur le Terminale et faire les commandes suivantes:

        sudo apt-get update
        
        Puis une fois finis :
        sudo apt install make gcc dkms linux-source linux-headers-$(uname -r)
        
        cd /media/cdrom0
        
        sudo sh VBoxLinuxAdditions.run
        



Une fois les commandes effectués:

- copier un fichier dans votre dossier partagé dans la machine virtuelle (Debian) puis vérifier depuis Windows que vous y avez accès
- copier un fichier dans votre dossier partagé dans la machine hôte (Windows) puis vérifier depuis Debian que vous y avez accès

#### __<span style='color:red'>faire en sorte que les 2 machines puisse discuter</span>__

La configuration nécessaire est :

1. ouvrir virtualBox 
2. configuration de la machine Debian, puis réseau interface 1 changer NAT en accès par pont 
3. puis avancé , mode promiscuité : autoriser la VMs

Pour vérifier si la manipulation marche:

1. allez dans le terminal sous Debian puis faire ip a 
2. repérer dans 2: enpos3 , l'adresse ip inet 
3. Puis allez dans le cmd de windows et marquez ping 172.16.96.222(adresse précédement repérer dans Debian ) si on obtient un résultat   




