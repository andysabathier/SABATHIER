__*Andy Sabathier*__ <br>
__*06/06/19*__ <br>
__*BTS SIO SLAM*__ <br>


# __*Activité 5: Serveur DHCP*__

### __*Étape 1: Préparer un réseau-test*__

créez 4 machines avec 512 Mo de mémoire vive <br>

- machine vierge avec DHCP
- machine vierge
- machine apache
- machine Mariadb

### __*Étape 2: Installer un serveur DHCP et Tshark*__