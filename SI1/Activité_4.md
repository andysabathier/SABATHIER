Andy SABATHIER

Bts Sio Slam

Activité 4 Terminal Linux  
=

Exo 1
-

Référencer le fichier track1.mp3

    /home/bob/music/rock/track1.mp3
chemin relatif à partir de /home/bob

    ~/music/rock/track1.mp3
chemin relatif à partir de /home/bob/images

    ../music/rock/track1.mp3
chemin relatif à partir de /home/bob/tmp/tests

    ../../music/rock/track1.mp3
se déplacer dans le rep rock

    cd rock
se déplacer dans le rep test

    cd
    cd tmp/test
Dans le rep perso de bob

    cd
A la racine de l'arboresence

    cd /
se déplacer dans le rep etc

    cd etc

Exo 2
-

pour faire l'arborescence de la question 1

    mkdir monsite
    cd monsite
    touch admin.html
    touch index.html
    mkdir css
    mkdir images
    mkdir lib
    cd css
    touch style.css
    cd ..
    cd lib
    mkdir bootstr
    cd ..

1) afficher le chemin

    pwd
2)fichier .gitignore dans le rep monsite

    touch .gitignore
    
3)afficher le rep monsite avec info détaillées

    ls -l
4)afficher le rep monsite avec les fichiers caché

    ls -al
5)copier le fichier index.html

    cp index.html index2.html
6) creer un rep archive

    mkdir archive
7)copier en une seule commande tous les fichier .html dans archive

    cp *.html archive/
8)renomer style.css en monsite.css

    cd css
    mv style.css monsite.css
    cd ..
    
9)suprimer les fichiers index1 et 2

    rm i*.html
10)supprimer le rep monsite 

    cd ..
    rm -r monsite
    
Exo 3)
-

1)Créer un répertoire exo3 .Vous y déplacer. Créer un fichier vide moi.txt dans le répertoire courant. Afficher le message “Je suis en BTS SIO”. Ecrire le message précédent dans le fichier moi.txt. Ajouter au fichier moi.txt la ligne “Et j’aime ça !”.

    mkdir exo3
    cd exo3
    touch moi.txt
    echo Je suis en BTS SIO
    echo Je suis en BTS SIO > moi.txt
    echo Et j\'aime ça ! >> moi.txt
    
2)En utilisant un éditeur de texte, créez le fichier rois.txt ayant le contenu suivant : 

François Ier, Henri IV, Louis XIV 

Créez aussi le fichier rio.txt ayant le contenu suivant : 

Rio de Janeiro est la deuxième plus grande ville du Brésil. 
    
    touch rois.txt
    gedit rois.txt
    François Ier
    Henri IV
    Louis XIV
    touch rio.txt
    gedit rio.txt
    Rio de Janeiro est la deuxième plus grande ville du Brésil.

3)Dupliquer rois.txt sous le nom roisFrance.txt. Déplacer tous les fichiers sauf rio.txt dans votre répertoire personnel. Revenir dans votre répertoire personnel. Supprimer les fichiers créés précédemment. Supprimer le répertoire exo3. Réinitialiser le contenu de la console. 
    
    cp rois.txt roisFrance.txt
    mv *oi*.txt ../
    cd ..
    rm *.txt rio.txt
    rm -r exo3
    clear

Exo 4)
-

1)Créer le répertoire bin dans votre répertoire personnel

    mkdir bin
2)Afficher la liste des variables d’environnement.

    env
3)Afficher le nom de l’utilisateur courant. 

    echo $USERNAME
4)Ajouter le répertoire bin créé précédemment à la variable PATH

    export PATH=$PATH/home/Andy/bin
5)Créer l’alias la qui lance la commande ls -a. 

    alias la="ls -a"


FIN
=


    
    


