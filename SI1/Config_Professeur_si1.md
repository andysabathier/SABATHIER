25 Octobre 2018
-

Voici les conseils de notre groupe pour le choix d'un ordinateur à usage bureautique
 (pour l'utilisation du pack office de Microsoft, de Youtube.. etc).

Nous avons également compris votre besoin bureautique. Nous vous conseillons de souscrire à un pack office (Applications Microsoft).
Voici ce que l'on vous propose pour environ 10€ : 

https://www.amazon.fr/Microsoft-professionnel-version-complète-uniquement/dp/B072R63CH7/ref=sr_1_6?ie=UTF8&qid=1540477272&sr=8-6&keywords=office+365


Pour répondre aux critères, nous proposons deux ordinateurs portables.

**Notre groupe :** 
Maxime GUICHARD,
Andy SABATHIER, 
Quentin NARDY

---
***Notre première sélection***
---
*PC Portable HP 17-by0133nf*

**Caractéristiques :**

| Composant | Modèle                                          |
| --------- | ----------------------------------------------: | 
| Processeur| Intel Core i3 (7ème génération) 7020U - 2.3 GHz |
| Ecran | 17.3" - 1600 x 900 (HD+) | 
| RAM  | 4 Go (1 x 4 Go) |
|Disque Dur | 500 Go HDD SATA - 5400 tours-min |
|Processeur graphique | Intel HD Graphics 620 |
|Autonomie de la batterie | Jusqu'à 11 heures |
| Interfaces (Branchements disponibles) | 2 x USB 3.1 Gen 1, USB 2.0, HDMI, LAN, Prise combo casque-microphone|
|Système d'exploitation | Windows 10 Edition Familiale 64 bits - Anglais- Français|

**Nos explications :**

L'utilisateur exige un ordinateur portable avec un écran de 17 pouces environ, pour un usage bureautique.
Cet ordinateur rempli toutes ces exigences : 

Tout d'abord, il possède un écran 17.3" pour le confort de l'utilisateur. 
Ensuite, les composants de celui-ci permettent l'utilisation aisée des logiciels Microsoft (tel que Excel ou Word) ou encore de Youtube et d'autres plateformes de vidéos.
La capacité de stockage de cet ordinateur est de 500 Go, ce qui est une bonne capacité de stockage. 
Si la capacité de stockage est insuffisante, nous conseillons d'investir dans un disque dur externe (pour 1 To de stockage, un disque coûte environ 40€).


**Prix :** 
Actuellement en promotion à 399€ /
Prix de base : 549€

**Lien du vendeur :** 
https://www.cdiscount.com/informatique/ordinateurs-pc-portables/hp-pc-portable-17-by0133nf-17-3-hd-intel-core-i3/f-10709-hp17by0133nf.html#read

-----
***Notre deuxième sélection***
---
*PC Lenovo 320-17AST 80XW* 

**Caractéristique :**

|Composant | Modèle |
| --- | ---:|
|Processeur |  AMD E2 9000 - 1.8 GHz |
| Ecran | 17.3" - TN - 1600 x 900 (HD+)| 
|RAM | 4 Go (1 x 4 Go)|
|Disque Dur | 1 To HDD SATA 6Gb-s|
|Processeur graphique | AMD Radeon R2|
|Autonomie de la batterie | Jusqu'à 5.5 heures|
|Interfaces | USB 3.0, USB 2.0, HDMI, LAN, Prise combo casque-microphone|
|Système d'exploitation | Windows 10 Edition Familiale 64 bits|

**Nos explications :**

C'est un ordinateur portable qui correspond au critères : écran de 17.3" avec la capacité d'utiliser les logiciels Microsoft. 

Celui-ci offre moins de puissance que le HP ci-dessus. En revanche, il possède une capacité de stockage supérieur sur son disque (1 To HDD SATA 6Gb-s).
Son point faible reste son autonomie de 5 heures 30 min. Pour contrer sa faible autonomie, il est possible de laisser brancher l'ordinateur sur secteur. 


**Prix :** 
393,17€ 
Il est possible de payer en 4 fois.

**Lien du vendeur :** https://www.cdiscount.com/informatique/ordinateurs-pc-portables/lenovo-320-17ast-80xw-e2-9000-1-8-ghz-win-10-fam/f-1070992-len0192076667765.html?idOffre=291368470#desc 