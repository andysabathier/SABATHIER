$(document).ready(function(){
 
    $("#submit").click(function(e){
        e.preventDefault();
 
        $.post(
            'connexion.php',
            {
                txtEmail : $("#txtEmail").val(),  // Nous récupérons la valeur de nos input que l'on fait passer à connexion.php
                mdp : $("#mdpPass").val()
            },
 
            function(data){
 
                if(data == 'Ok'){
                     // Le membre est connecté. Ajoutons lui un message dans la page HTML.
                     $("#resultat").html("<p><br>Vous avez été connecté avec succès !</p>");
                }
                else{
                     // Le membre n'a pas été connecté. (data vaut ici "fail")
 
                     $("#resultat").html("<p><br>Erreur lors de la connexion...</p>");
                }
         
            },
        );
    });
});
