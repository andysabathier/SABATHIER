<?php

    class Maison
    {
        private $house_name;
        private $devise;

		private $armoirie;
		private $date_fondation;
		private $region;  
		
		public function __construct($house_name, $devise, $armoirie, $date_fondation, $region)
        {
        	$this->house_name =$house_name;
            $this->devise =$devise;
            $this->armoirie =$armoirie;
            $this->date_fondation =$date_fondation;
            $this->region =$region;
        }
  
        public function getHouse_name()   
        {      
            return $this->house_name;        
        }   
        public function set_house_name($var) 
        {  
            $this->house_name=$var;
        }


        public function getDevise()   
        {      
            return $this->devise;        
        }   
        public function setDevise($var) 
        {  
            $this->devise=$var;
        }



        public function getArmoirie()   
        {      
            return $this->armoirie;        
        }   
        public function setArmoirie($var) 
        {  
            $this->armoirie=$var;
        }


        public function getDate_fondation()   
        {      
            return $this->date_fondation;        
        }   
        public function setDate_fondation($var) 
        {  
            $this->date_fondation=$var;
        }


        public function getRegion()   
        {      
            return $this->region;        
        }   
        public function setRegion($var) 
        {  
            $this->region=$var;
        }

    } 

?>