<?php
    include_once "maison.php";
    include_once "characters.php";

    class Noble extends Characters
    {
        private $marie;
        private $pere;
        private $mere;

        private $maison;
        private $nom;

 
        public function __construct($identifiant, $nom, $born_date, $die_date, $culture, $genre, $marie, $pere, $mere, $maison, $nom)
        {
            parent::__construct($identifiant, $nom, $born_date, $die_date, $culture, $genre);
            $this->marie= $marie;
            $this->pere= $pere;
            $this->mere= $mere;
            $this->maison= $maison;
            $this->nom= $nom;
        }

        public function __toString() {

            return parent::__toString(). ' Le marie est :' .$this->marie.'<br>' .'Le pere est :'.$this->pere.'<br>' . 'la mère est: '.$this->mere . '<br>' .'la maison est : '. $this->maison. '<br>' .'le nom est :'.$this->nom . '<br>' ;

        }

        public function getMarie()   
        {      
            return $this->marie;        
        }   
        public function setMarie($var) 
        {  
            $this->marie=$var;
        }


        public function getPere()   
        {      
            return $this->pere;        
        }   
        public function setPere($var) 
        {  
            $this->pere=$var;
        }



        public function getMere()   
        {      
            return $this->mere;        
        }   
        public function setMere($var) 
        {  
            $this->mere=$var;
        }



        public function getMaison()   
        {      
            return $this->maison;        
        }   
        public function setMaison($var) 
        {  
            $this->maison=$var;
        }


        public function getNom()   
        {      
            return $this->nom;        
        }   
        public function setNom($var) 
        {  
            $this->nom=$var;
        }


    } 

?>


