
<?php
    abstract class Characters
    {
        private $identifiant;
        private $nom;

		private $born_date;
		private $die_date; 
		private $culture;
		private $genre;
        public static $cpt = 0;


		public function __construct( $identifiant, $nom, $born_date, $die_date, $culture, $genre)
        {
        	$this->identifiant = $identifiant;
            $this->nom = $nom;
            $this->born_date = $born_date;
            $this->die_date = $die_date;
            $this->culture = $culture;
            $this->genre = $genre;
            Characters::$cpt++;
        }

        public function __toString() {

            return 'L identifiant est :' .$this->identifiant.'Le nom est :'.$this->nom. 'la date de naissance : '.$this->born_date.'la date de mort : '. $this->die_date.'la culture :'.$this->culture.'le genre :'.$this->genre;

        }

        public function get_Identifiant()   
        {      
            return $this->identifiant;        
        }   
        public function set_Identifiant($var) 
        {  
            $this->identifiant=$var;
        }


        public function get_Nom()   
        {      
            return $this->nom;        
        }   
        public function set_Nom($var) 
        {  
            $this->nom=$var;
        }

        public function get_Born_date()   
        {      
            return $this->born_date;        
        }   
        public function set_Born_date($var) 
        {  
            $this->born_date=$var;
        }



        public function get_Die_date()   
        {      
            return $this->die_date;        
        }   
        public function set_Die_date($var) 
        {  
            $this->die_date=$var;
        }


        public function get_Culture()   
        {      
            return $this->culture;        
        }   
        public function set_Culture($var) 
        {  
            $this->culture=$var;
        }


        public function get_Genre()   
        {      
            return $this->genre;        
        }   
        public function set_Genre($var) 
        {  
            $this->genre=$var;
        }

    } 

?>